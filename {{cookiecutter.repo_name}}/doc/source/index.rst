.. {{ cookiecutter.project_name }} documentation master file

{{ cookiecutter.project_name }}
**************************

by {{ cookiecutter.author_name }}

.. toctree::
   :maxdepth: 2
   :caption: Inhalt:

   
   pages/introduction


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
